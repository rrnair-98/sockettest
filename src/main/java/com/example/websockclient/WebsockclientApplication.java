package com.example.websockclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


import org.glassfish.tyrus.client.ClientManager;

import java.io.IOException;
import java.net.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import javax.websocket.DeploymentException;
import javax.websocket.ClientEndpointConfig;
import javax.websocket.Endpoint;
import javax.websocket.Session;
import javax.websocket.MessageHandler;
import javax.websocket.EndpointConfig;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
@SpringBootApplication
public class WebsockclientApplication {
    //private static final String TOKEN = "eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoiMmVmMDNiNjgtYzcyNy00NmE4LWEzNGQtNmMwYTUzYTRjN2I5IiwiZXhwIjoxNTYxNTU5NDY1fQ.mUB48R81b-W0pE-wTh1U_C1qgU51cAmbXr19epS_ktQ";
    private static CountDownLatch messageLatch;
    private static final String SENT_MESSAGE = "Hello World";
    private final static String PROD_URL = "wss://login.staging.gocomet.com/cable/";
    private final static String LOCAL_URL = "ws://lvh.me:3000/cable";
	private static String T;
  public static void main(String[] args) {
    try {
      T = args[0];
            messageLatch = new CountDownLatch(1);

            final ClientEndpointConfig cec = ClientEndpointConfig.Builder.create()
    .configurator(new ClientEndpointConfig.Configurator() {
        @Override
        public void beforeRequest(Map<String, List<String>> headers) {
            super.beforeRequest(headers);
            List<String> cookieList = headers.get("Cookie");
            if (null == cookieList) {
                cookieList = new ArrayList<>();
            }
            cookieList.add("token="+T);
            headers.put("Cookie", cookieList);
        }
    }).build();

            ClientManager client = ClientManager.createClient();
            client.connectToServer(new Endpoint() {

                @Override
                public void onOpen(Session session, EndpointConfig config) {
                    try {
                        session.addMessageHandler(new MessageHandler.Whole<String>() {

                            @Override
                            public void onMessage(String message) {
                                System.out.println("Received message: "+message);
                                messageLatch.countDown();
                            }
                        });
                        session.getBasicRemote().sendText(SENT_MESSAGE);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }, cec, new URI(PROD_URL));
            messageLatch.await(100, TimeUnit.SECONDS);
        } catch (Exception e) {
            e.printStackTrace();
        }

	}

}
